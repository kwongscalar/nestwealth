To run this Ansible playbook locally, use the following command:

On SFTP Server:
ansible-playbook -i localhost, -c local sftp.yml

On MongoDB Servers:
ansible-playbook -i localhost, -c local mongodb.yml

To generate Git hash:
git rev-parse HEAD >> git-commit.txt

# Nestwealth Financial Production Website

This is the Terraform and Ansible to build the servers required for the following domains:  

* nestwealth.com

The servers are built with Terraform and use an AMI (pre-built by Nest) which includes NodeJS, pm2, git.  The AMI was based off of the latest Amazon Linux distribution at the time of creation.

### External Git Repositories

None of the Git repositories are public facing.  They will need to be
accessed from the Nestwealth servers which have the correct public key enabled.

https://github.com/nestwealth

```
API:
join api: git@github.com:nestwealth/join_api.git
port api: git@github.com:nestwealth/portfolio_api.git

Web:
join web: git@github.com:nestwealth/join_webserver.git
admin web: git@github.com:nestwealth/admin_webserver.git
port web: git@github.com:nestwealth/portfolio_webserver.git
com web: git@github.com:nestwealth/com_webserver.git
svg web: git@github.com:nestwealth/svg_webserver.git

Key server:
key server: git@github.com:nestwealth/key_server.git

Submodules:
email webserver: git@github.com:nestwealth/email_webserver.git
ui toolkit: git@github.com:nestwealth/nwui-toolkit-angular.git
```

## Troubleshooting

There is only one main process which would cause an issue (nodejs).  SSH to each of the servers (using ec2-user) and try the following commands:

### Restart nodejs

```
cd ~ec2-user/<git-repo>
NODE_ENV=production NODE_CONFIG_ENV=production DEBUG=app:app.js pm2 start app.js
pm2 save
```

## Code Deployment

Code deploy will be handled by Nestwealth developers at this point.  The process is typically like this:

1. Login to each of the servers and cd to the Git repo.
2. Pull down the release branch or pull from latest master.
3. Restart nodejs process.

## Architecture

![alt text](/documentation/TIFF-CanadaOnScreen-Website-Architecture.png "TIFF-COS-Architecture")

### Servers
There are two servers sitting in an auto-scaling group which just scale up based on CPU.

### Storage
Nestwealth has many S3 buckets in use for their application, but the only one used for this particular
deployment is the "nestweath-ansible" bucket sitting in the Scalar labs account.
```
* nestwealth-ansible  (used for installing & configuring MongoDB & SFTP servers)
```

### Networking
The servers for the nestweath.com website sits within a VPC (Canada Central region) in their own AWS account.  Outbound access for the servers in private subnets uses the managed NAT gateways.

VPC Name: nestwealth-prod-vpc
VPC CIDR: 10.21.0.0/16

Public subnet1: 10.21.1.0/24
Public subnet2: 10.21.2.0/24

Private app subnet1: 10.21.10.0/24
Private app subnet2: 10.21.20.0/24

Private db subnet1: 10.21.30.0/24
Private db subnet2: 10.21.40.0/24

### Content Distribution Network (CDN)

No CDN in use at this time

### DNS

Hosted outside of AWS at this time.  There are plans to move the zones at some point.

### SSL Certficates

SSL certificates were originally with GoDaddy and re-generated with AWS Certificate Manager.

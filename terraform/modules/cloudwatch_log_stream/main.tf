variable "environment" {
  description = "Environment name"
}

variable "client_name" {
  description = "Client name"
}

variable "cloudwatch_log_stream_name" {
  description = "The name of the Cloudwatch log stream"
}

variable "cloudwatch_log_group_name" {
  description = "The name of the Cloudwatch log group under which the logs stream is to be created"
}

resource "aws_cloudwatch_log_stream" "client_name_cloudwatch_log_stream" {
  name              = "${var.client_name}-${var.environment}-${var.cloudwatch_log_stream_name}"
  log_group_name    = "${var.cloudwatch_log_group_name}"
}
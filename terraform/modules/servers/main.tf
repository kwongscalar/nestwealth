# Variables for instance
variable "instance_type" { default = "t2.small" }
variable "server_count" { default = "1" }
variable "key_name" {}
variable "client_name" {}
variable "environment" {}
variable "security_groups" {}
variable "associate_public_ip" { default = "false" }
variable "source_dest_check" { default = "true" }
variable "subnet_id" {}
variable "server_role" {}
variable "user_data_template" { default = "user_data_base_template"}
variable "root_volume_type" { default = "gp2" }
variable "root_volume_size" { default = "10" }
variable "delete_on_termination" { default = "true" }
variable "create_eip" { default = false }
variable "ami_filter_value" { default = "amzn-ami-hvm-*x86_64-gp2" }
variable "ami_owner" { default = "137112412989" }
variable "aws_login_user" { default = "ec2-user" }
variable "ansible_root_dir" { default = "/etc/ansible" }
variable "s3_bucket" { default = "nestwealth-ansible" }
variable "ansible_tar_file" {}
variable "ansible_playbook" {}

# NOTE: this module uses the pre-baked AMI provided by Nestwealth.
data "aws_ami" "amzn_ami" {
  most_recent  = true
  filter {
    name   = "name"
    values = ["${var.ami_filter_value}"]
  }
  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }
  owners = ["${var.ami_owner}"]
}

data "aws_region" "current" {
  current = true
}

# Use this template_file to inject variables to Bash script
data "template_file" "server_user_data" {
  template = "${file(var.user_data_template)}"

  vars {
    aws_region       = "${data.aws_region.current.name}"
    aws_login_user   = "${var.aws_login_user}"
    ansible_root_dir = "${var.ansible_root_dir}"
    s3_bucket        = "${var.s3_bucket}"
    ansible_tar_file = "${var.ansible_tar_file}"
    ansible_playbook = "${var.ansible_playbook}"
  }
}

# Create EIP for bastion host.  We use the count to simulate an if-statement.
# In Terraform, a boolean true is converted to a 1, and false is converted to a 0.
# Knowing this, we can use the count variable to act as a conditional.
resource "aws_eip" "server_eip" {
  count    = "${var.create_eip}"
  instance = "${aws_instance.server.id}"
  vpc      = true
}

# Setup an instance
resource "aws_instance" "server" {
  ami                         = "${data.aws_ami.amzn_ami.id}" 
  instance_type               = "${var.instance_type}"
  count                       = "${var.server_count}"  
  key_name                    = "${var.key_name}"
  iam_instance_profile        = "${var.client_name}_generic_instance_profile"
  vpc_security_group_ids      = ["${split(",", var.security_groups)}"]
  #associate_public_ip_address = "${var.associate_public_ip}"
  source_dest_check           = "${var.source_dest_check}"
  subnet_id                   = "${element(split(",", var.subnet_id), count.index)}"
  user_data                   = "${data.template_file.server_user_data.rendered}"

  root_block_device {
    volume_type           = "${var.root_volume_type}"
    volume_size           = "${var.root_volume_size}"
    delete_on_termination = "${var.delete_on_termination}"
  }

  tags {
    Name = "${var.client_name}-${var.environment}-${var.server_role}"
    Terraform  = "true"    
    Role = "${var.client_name}-${var.server_role}"
  }
}


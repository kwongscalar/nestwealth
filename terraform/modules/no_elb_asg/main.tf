# Variables 
variable "client_name" {}
variable "vpc_cidr" {}
variable "environment" {}
variable "security_groups" {}
variable "server_role" {}

# Variables for launch configuration
variable "instance_type" { default = "t2.small" }
variable "key_name" {}
variable "ebs_optimized" { default = "false" }
variable "enable_monitoring" { default = "false" }
variable "associate_public_ip" { default = "false" }
variable "root_volume_type" { default = "gp2" }
variable "root_volume_size" { default = "30" }
variable "ami_filter_value" { default = "prod_base_*" }
variable "ami_owner" { default = "841968689972" }
variable "user_data_script" { default = "user_data.sh"}

# Variables for autoscaling group
variable "asg_subnets" {}
variable "asg_max_size" {}
variable "asg_min_size" {}
variable "asg_health_check_grace_period" { default = "300" }
variable "asg_health_check_type" { default = "EC2" }
variable "asg_desired_capacity" {}
variable "asg_termination_policy" { default = "OldestInstance"}


# Find the custom AMI by region
data "aws_ami" "custom_nodejs_ami" {
  most_recent = true

  filter {
    name   = "name"
    #values = ["uat_base_*"]
    values = ["${var.ami_filter_value}"]

  }

  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }

  # Owned by Nest Wealth
  owners = ["${var.ami_owner}"]
}

# Setup launch configuration
resource "aws_launch_configuration" "client_name_launch_config" {
  name_prefix                 = "${var.client_name}-${var.environment}-launch_config-${var.server_role}-"
  image_id                    = "${data.aws_ami.custom_nodejs_ami.id}"
  instance_type               = "${var.instance_type}"
  key_name                    = "${var.key_name}"
  iam_instance_profile        = "${var.client_name}_generic_instance_profile"
  security_groups             = ["${split(",", var.security_groups)}"]
  user_data                   = "${file(var.user_data_script)}"
  ebs_optimized               = "${var.ebs_optimized}"
  enable_monitoring           = "false"
  associate_public_ip_address = "${var.associate_public_ip}"

  root_block_device {
    volume_type = "${var.root_volume_type}"
    volume_size = "${var.root_volume_size}"
  }

  lifecycle {
    create_before_destroy = true
  }
}

# Setup scaling group
resource "aws_autoscaling_group" "client_name_autoscaling_group" {
  name                      = "${var.client_name}_${var.environment}_asg_${var.server_role}"
  max_size                  = "${var.asg_max_size}"
  min_size                  = "${var.asg_min_size}"
  health_check_grace_period = "${var.asg_health_check_grace_period}"
  health_check_type         = "${var.asg_health_check_type}"
  desired_capacity          = "${var.asg_desired_capacity}"
  termination_policies      = ["${var.asg_termination_policy}"]
  launch_configuration      = "${aws_launch_configuration.client_name_launch_config.name}"
  vpc_zone_identifier       = ["${split(",", var.asg_subnets)}"]

  tag {
    key                 = "Name"
    value               = "${var.client_name}-${var.environment}-asg-${var.server_role}"
    propagate_at_launch = true
  }

  tag {
    key                 = "Terraform"
    value               = "true"
    propagate_at_launch = true
  }  
}

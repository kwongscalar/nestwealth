# Variables for bastion
variable "instance_type" {}
variable "key_name" {}
variable "client_name" {}
variable "environment" {}
variable "source_dest_check" { default = "true" }
variable "security_groups" {}
variable "subnet_id" {}
variable "server_role" {}



# Choose the correct AMI based on region
data "aws_ami" "amzn_ami" {
  most_recent = true

  filter {
    name   = "name"
    values = ["amzn-ami-hvm-*x86_64-gp2"]
  }

  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }

  # Owned by Amazon
  owners = ["137112412989"]
}

# Create EIP for bastion host
resource "aws_eip" "eip_bastion" {
  instance = "${aws_instance.bastion_host.id}"
  vpc      = true
}

# Setup a bastion host
resource "aws_instance" "bastion_host" {
  ami                         = "${data.aws_ami.amzn_ami.id}"
  instance_type               = "${var.instance_type}"
  key_name                    = "${var.key_name}"
  iam_instance_profile        = "${var.client_name}_generic_instance_profile"
  vpc_security_group_ids      = ["${split(",", var.security_groups)}"]
  source_dest_check           = "${var.source_dest_check}"
  subnet_id                   = "${var.subnet_id}"
  #user_data                   = "${file("user_data.sh")}"

  tags {
    Name = "${var.client_name}-${var.environment}-${var.server_role}"
    Terraform  = "true"
  }
}

variable "environment" {
  description = "Environment name"
}

variable "client_name" {
  description = "Client name"
}

variable "subnet_id" {
  description = "The DB subnet ranges"
}

resource "aws_db_subnet_group" "default" {
  name       = "${var.client_name}-${var.environment}-db_subnet-group"
  subnet_ids = ["${split(",", var.subnet_id)}"]

  tags {
    Name = "${var.client_name}-${var.environment}-db-subnet-group"
    Terraform  = "true"  
  }
}
variable "region" {
  description = "The region"
}

variable "environment" {
  description = "Environment name"
}

variable "client_name" {
  description = "Client name"
}

variable "cloudwatch_log_group_name" {
  description = "The name of the Cloudwatch log group"
}

variable "s3_bucket_name" {
  description = "The value which precedes the name of the S3 bucket"
}

variable "include_global_service_events" {
  description = "Specifies whether the trail is publishing events from global services to logs"
  default     = true
}

variable "enable_lifecycle_rule" {
  description = "Specifies whether the S3 lifecycle rule should be enabled or not"
  default     = true
}

variable "cloudtrail_days_before_log_expiry" {
    description = "The number of days before Cloudtrail logs expire in the S3 bucket"
    default     = 30
}

# Turn Cloudtrail on and provide S3 bucket for log shipping
resource "aws_cloudtrail" "client_name_cloudtrail" {
    name                          = "${var.client_name}-${var.environment}-cloudtrail"
    s3_bucket_name                = "${aws_s3_bucket.client_name_s3_bucket.id}"
    include_global_service_events = "${var.include_global_service_events}"
}

# Create an S3 bucket for the Cloudtrail logs and add correct lifecycle and bucket policies
resource "aws_s3_bucket" "client_name_s3_bucket" {
    bucket = "${var.client_name}-${var.environment}-${var.s3_bucket_name}"
    force_destroy = true

    lifecycle_rule {
        id      = "${var.client_name}-${var.environment}-lifecycle-rule"
        prefix  = ""
        enabled = "${var.enable_lifecycle_rule}"
        expiration {
            days = "${var.cloudtrail_days_before_log_expiry}"
        }
    }
    policy = <<POLICY
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Sid": "AWSCloudTrailAclCheck",
            "Effect": "Allow",
            "Principal": {
              "Service": "cloudtrail.amazonaws.com"
            },
            "Action": "s3:GetBucketAcl",
            "Resource": "arn:aws:s3:::${var.s3_bucket_name}"
        },
        {
            "Sid": "AWSCloudTrailWrite",
            "Effect": "Allow",
            "Principal": {
              "Service": "cloudtrail.amazonaws.com"
            },
            "Action": "s3:PutObject",
            "Resource": "arn:aws:s3:::${var.s3_bucket_name}/*",
            "Condition": {
                "StringEquals": {
                    "s3:x-amz-acl": "bucket-owner-full-control"
                }
            }
        }
    ]
}
POLICY
}


# Create IAM role for Cloudtrail.  Used for notifying Cloudwatch.
resource "aws_iam_role" "client_name_cloudtrail_role" {
  name               = "${var.client_name}-${var.environment}-cloudtrail-role"
  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Sid": "",
      "Effect": "Allow",
      "Principal": {
        "Service": "cloudtrail.amazonaws.com"
      },
      "Action": "sts:AssumeRole"
    }
  ]
}
EOF
}

# Find current account ID
data "aws_caller_identity" "current" { }

# Create IAM policy to allow Cloudtrail to create a Cloudwatch log stream in specified log group.
resource "aws_iam_policy" "client_name_cloudtrail_stream_policy" {
  name        = "${var.client_name}-${var.environment}-cloudtrail_stream_policy"
  path        = "/"
  description = "${var.client_name}-${var.environment}-cloudtrail-stream_policy"
  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {

      "Sid": "AWSCloudTrailCreateLogStream2014110",
      "Effect": "Allow",
      "Action": [
        "logs:CreateLogStream"
      ],
      "Resource": [
        "arn:aws:logs:${var.region}:${data.aws_caller_identity.current.account_id}:log-group:${var.cloudwatch_log_group_name}:log-stream:${data.aws_caller_identity.current.account_id}_CloudTrail_${var.region}*"
      ]

    },
    {
      "Sid": "AWSCloudTrailPutLogEvents20141101",
      "Effect": "Allow",
      "Action": [
        "logs:PutLogEvents"
      ],
      "Resource": [
        "arn:aws:logs:${var.region}:${data.aws_caller_identity.current.account_id}:log-group:${var.cloudwatch_log_group_name}:log-stream:${data.aws_caller_identity.current.account_id}_CloudTrail_${var.region}*"
      ]
    }
  ]
}
EOF
}

# Attach Cloudtrail policy to role
resource "aws_iam_role_policy_attachment" "client_name_attach_cloudtrail_stream_policy" {
  role       = "${aws_iam_role.client_name_cloudtrail_role.name}"
  policy_arn = "${aws_iam_policy.client_name_cloudtrail_stream_policy.arn}"
}
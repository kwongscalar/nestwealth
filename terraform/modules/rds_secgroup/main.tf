# Variable inputs to module
variable "vpc_cidr" {
  description = "Subnet of VPC"
}

variable "vpc_id" {
  description = "The ID of the VPC"
}

variable "environment" {
  description = "Environment name"
}

variable "client_name" {
  description = "Client name"
}

variable "security_group_bastion" {
  description = "The bastion SG id"
}

variable "security_group_web" {
  description = "The web SG id"
}

variable "security_group_api" {
  description = "The api SG id"
}

variable "allowed_subnets" {
  description = "The subnets for access"
}

/* Security group for RDS */
resource "aws_security_group" "client_name_rds_security_group" {
  name        = "${var.client_name}_${var.environment}_rds_security_group"
  description = "Allow MySQL port 3306, MongoDB port 27017 and Redis port 6379 from VPC"
  vpc_id      = "${var.vpc_id}"

  ingress {
    from_port       = 22
    to_port         = 22
    protocol        = "tcp"
    security_groups = ["${var.security_group_bastion}"]
    #cidr_blocks     = ["${var.vpc_cidr}"]
    self            = false
  }

  ingress {
    from_port       = 6379
    to_port         = 6379
    protocol        = "tcp"
    cidr_blocks     = ["${split(",", var.allowed_subnets)}"]
    self            = false
  }

  # ingress {
  #   from_port       = 3306
  #   to_port         = 3306
  #   protocol        = "tcp"
  #   security_groups = ["${var.security_group_api}"]
  #   self            = false
  # }

  ingress {
    from_port       = 27017
    to_port         = 27017
    protocol        = "tcp"
    #cidr_blocks     = ["${split(",", var.allowed_subnets)}"]        
    security_groups = ["${var.security_group_web}"]
    self            = false
  }

  ingress {
    from_port       = 3306
    to_port         = 3306
    protocol        = "tcp"
    cidr_blocks     = ["${split(",", var.allowed_subnets)}"]        
    #security_groups = ["${var.security_group_api}"]
    self            = false
  }

  egress {
    from_port       = 80
    to_port         = 80
    protocol        = "tcp"
    cidr_blocks     = ["0.0.0.0/0"]
  }

  egress {
    from_port       = 443
    to_port         = 443
    protocol        = "tcp"
    cidr_blocks     = ["0.0.0.0/0"]
  }

  tags {
    Name = "${var.client_name}-${var.environment}-rds-security-group"
    Terraform  = "true"    
  }
}

variable "environment" {
  description = "Environment name"
}

variable "client_name" {
  description = "Client name"
}

variable "cloudwatch_log_group_name" {
  description = "The name of the Cloudwatch log group"
}

variable "retention_in_days" {
	description = "The retention time for the logs (days)"
	default     = "7"
}

resource "aws_cloudwatch_log_group" "client_name_cloudwatch_log_group" {
  name              = "${var.client_name}-${var.environment}-${var.cloudwatch_log_group_name}"
  retention_in_days = "${var.retention_in_days}"
  tags {
    Name       = "${var.client_name}-${var.environment}-${var.cloudwatch_log_group_name}"
    Terraform  = "true"
  }
}
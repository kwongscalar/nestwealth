output "mongo_instance_id" {
 value = "${aws_instance.mongodb.id}"
}

output "mongo_private_ip" {
 value = "${aws_instance.mongodb.private_ip}"
}

output "mongo_private_dns" {
 value = "${aws_instance.mongodb.private_dns}"
}

# Variables for mongo servers
variable "instance_type" {}
variable "key_name" {}
variable "client_name" {}
variable "associate_public_ip" { default = "false" }
variable "environment" {}
variable "source_dest_check" { default = "true" }
variable "security_groups" {}
variable "subnet_id" {}
variable "server_role" {}
variable "server_count" { default = "1" }
variable "disk_type" { default = "io1" }
variable "mongo_data_disk_size" {}
variable "mongo_data_iops" { default = "1000"}
variable "mongo_journal_disk_size" {}
variable "mongo_journal_iops" { default = "250"}
variable "mongo_logs_disk_size" {}
variable "mongo_logs_iops" { default = "100"}
variable "user_data_script" { default = "user_data_mongo.sh"}



# Choose the correct AMI based on region
data "aws_ami" "amzn_ami" {
  most_recent = true

  filter {
    name   = "name"
    values = ["amzn-ami-hvm-*x86_64-gp2"]
  }

  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }

  # Owned by Amazon
  owners = ["137112412989"]
}

# Setup a mongodb cluster
# The subnet_id line is effectively a loop.  Choosing amongst the 
# list of subnets passed in to distribute servers across AZs.
resource "aws_instance" "mongodb" {
  ami                         = "${data.aws_ami.amzn_ami.id}"
  instance_type               = "${var.instance_type}"
  count                       = "${var.server_count}"
  key_name                    = "${var.key_name}"
  iam_instance_profile        = "${var.client_name}_generic_instance_profile"
  vpc_security_group_ids      = ["${split(",", var.security_groups)}"]
  associate_public_ip_address = "${var.associate_public_ip}"
  source_dest_check           = "${var.source_dest_check}"
  subnet_id                   = "${element(split(",", var.subnet_id), count.index)}"
  user_data                   = "${file(var.user_data_script)}"

  tags {
    Name = "${var.client_name}-${var.environment}-${var.server_role}"
    Terraform  = "true"    
  }
}

#Figure out AZ based on the subnet_id. EBS volumes require an AZ instead.
data "aws_subnet" "selected" {
  id = "${element(split(",", var.subnet_id), count.index)}"
}

resource "aws_ebs_volume" "mongo_data" {
  availability_zone = "${data.aws_subnet.selected.availability_zone}"
  type              = "${var.disk_type}"  
  size              = "${var.mongo_data_disk_size}"
  iops              = "${var.mongo_data_iops}"
  tags { Name = "${var.client_name}-${var.environment}-${var.server_role}-data"}
  tags { Terraform = "true" }
}

resource "aws_ebs_volume" "mongo_journal" {
  availability_zone = "${data.aws_subnet.selected.availability_zone}"
  type              = "${var.disk_type}"  
  size              = "${var.mongo_journal_disk_size}"
  iops              = "${var.mongo_journal_iops}"
  tags { Name = "${var.client_name}-${var.environment}-${var.server_role}-journal"}
  tags { Terraform = "true" }
}

resource "aws_ebs_volume" "mongo_logs" {
  availability_zone = "${data.aws_subnet.selected.availability_zone}"
  type              = "${var.disk_type}"  
  size              = "${var.mongo_logs_disk_size}"
  iops              = "${var.mongo_logs_iops}"  
  tags { Name = "${var.client_name}-${var.environment}-${var.server_role}-logs"}
  tags { Terraform = "true" }
}

resource "aws_volume_attachment" "mongo_data_att" {
  device_name  = "/dev/sdf"
  volume_id    = "${aws_ebs_volume.mongo_data.id}"
  instance_id  = "${aws_instance.mongodb.id}"
  skip_destroy = true
}

resource "aws_volume_attachment" "mongo_journal_att" {
  device_name  = "/dev/sdg"
  volume_id    = "${aws_ebs_volume.mongo_journal.id}"
  instance_id  = "${aws_instance.mongodb.id}"
  skip_destroy = true
}

resource "aws_volume_attachment" "mongo_logs_att" {
  device_name = "/dev/sdh"
  volume_id   = "${aws_ebs_volume.mongo_logs.id}"
  instance_id = "${aws_instance.mongodb.id}"
  skip_destroy = true  
}

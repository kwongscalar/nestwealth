output "elb_security_group_id" {
  value = "${aws_security_group.client_name_elb_security_group.id}"
}

output "elb_security_group_name" {
  value = "${aws_security_group.client_name_elb_security_group.name}"
}

output "elb_security_group_aligned_id" {
  value = "${aws_security_group.client_name_elb_security_group_aligned.id}"
}

output "elb_security_group_aligned_name" {
  value = "${aws_security_group.client_name_elb_security_group_aligned.name}"
}

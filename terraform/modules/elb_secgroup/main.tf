variable "vpc_id" {
  description = "The interpolated VPC ID"
}

variable "environment" {
  description = "Environment name"
}

variable "client_name" {
  description = "Client name"
}

variable "ips_allowed_to_elb" {
  description = "IP addresses allowed to ELB"
}

/* Security group for elbs */
resource "aws_security_group" "client_name_elb_security_group" {
  name        = "${var.client_name}_${var.environment}_elb_security_group"
  description = "Allow port 80 and 443 from specified IPs"
  vpc_id      = "${var.vpc_id}"
  
  ingress {
    from_port   = "80"
    to_port     = "80"
    protocol    = "tcp"
    cidr_blocks = ["${split(",", var.ips_allowed_to_elb)}"]
  }

  ingress {
    from_port   = "443"
    to_port     = "443"
    protocol    = "tcp"
    cidr_blocks = ["${split(",", var.ips_allowed_to_elb)}"]
  }

  egress {
    from_port   = "0"
    to_port     = "0"
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags {
    Name = "${var.client_name}-${var.environment}-elb-security-group"
    Terraform  = "true"        
  }
}

/* Security group for elbs */
resource "aws_security_group" "client_name_elb_security_group_aligned" {
  name        = "${var.client_name}_${var.environment}_elb_security_group_aligned"
  description = "Allow port 80 and 443 from specified IPs"
  vpc_id      = "${var.vpc_id}"
  
  ingress {
    from_port   = "80"
    to_port     = "80"
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port   = "443"
    to_port     = "443"
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = "0"
    to_port     = "0"
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags {
    Name = "${var.client_name}-${var.environment}-elb-security-group-aligned"
    Terraform  = "true"        
  }
}

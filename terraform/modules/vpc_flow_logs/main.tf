variable "vpc_id" {
  description = "The interpolated VPC ID"
}

variable "environment" {
  description = "Environment name"
}

variable "client_name" {
  description = "Client name"
}

variable "cloudwatch_log_group_name" {
  description = "The name of the Cloudwatch log group"
}

variable "iam_role_arn" {
  description = "The name of the Cloudwatch log group"
}

variable "traffic_type" {
	description = "The type of traffic to capture. ACCEPT, REJECT or ALL"
	default     = "ALL"
}


# Create the actual VPC flow log
resource "aws_flow_log" "client_name_vpc_flow_log" {
  log_group_name = "${var.cloudwatch_log_group_name}"
  iam_role_arn   = "${var.iam_role_arn}"
  vpc_id         = "${var.vpc_id}"
  traffic_type   = "${var.traffic_type}"
}
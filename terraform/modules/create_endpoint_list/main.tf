# Write outputs to a file for further configuration

 variable "client_name" {}
 variable "environment" {}
 variable "join_web_elb" {}
 variable "join_api_elb" {}
 variable "port_web_elb" {}
 variable "port_api_elb" {}
 variable "admin_web_elb" {}
 variable "config_api_elb" {}
 variable "mongodb_master_ip" {}
 variable "mongodb_replica1_ip" {}
 variable "mongodb_replica2_ip" {}
 variable "sftp_server_ext_ip" {}
 variable "sftp_server_public_dns" {}
 variable "redis_hostname" {}
 variable "redis_port" {}



resource "null_resource" "output-endpoints" {
  
  #Create join-web inventory
  provisioner "local-exec" {
    command =  "echo \"[join-web-elb]\n${var.join_web_elb}\" > ${var.client_name}.${var.environment}.endpoints"
  }

  #Create join-api inventory
  provisioner "local-exec" {
    command =  "echo \"\n[join-api-elb]\n${var.join_api_elb}\" >> ${var.client_name}.${var.environment}.endpoints"
  }

  #Create port-web inventory
  provisioner "local-exec" {
    command =  "echo \"\n[port-web-elb]\n${var.port_web_elb}\" >> ${var.client_name}.${var.environment}.endpoints"
  }

  #Create port-api inventory
  provisioner "local-exec" {
    command =  "echo \"\n[port-api-elb]\n${var.port_api_elb}\" >> ${var.client_name}.${var.environment}.endpoints"
  }

  #Create admin-web inventory
  provisioner "local-exec" {
    command =  "echo \"\n[admin-web-elb]\n${var.admin_web_elb}\" >> ${var.client_name}.${var.environment}.endpoints"
  }

  #Create config-api inventory
  provisioner "local-exec" {
    command =  "echo \"\n[config-api-elb]\n${var.config_api_elb}\" >> ${var.client_name}.${var.environment}.endpoints"
  }

  #Create mongodb master inventory
  provisioner "local-exec" {
    command =  "echo \"\n[mongodb_master]\n${var.mongodb_master_ip}\" >> ${var.client_name}.${var.environment}.endpoints"
  }

  #Create mongodb replica1 inventory
  provisioner "local-exec" {
    command =  "echo \"\n[mongodb_replica1]\n${var.mongodb_replica1_ip}\" >> ${var.client_name}.${var.environment}.endpoints"
  }

  #Create mongodb replica2 inventory
  provisioner "local-exec" {
    command =  "echo \"\n[mongodb_replica2]\n${var.mongodb_replica2_ip}\" >> ${var.client_name}.${var.environment}.endpoints"
  }

 #Create Redis inventory
  provisioner "local-exec" {
    command =  "echo \"\n[redis_endpoint]\n${var.redis_hostname}:${var.redis_port}\" >> ${var.client_name}.${var.environment}.endpoints"
  }

 #Create sftp inventory
  provisioner "local-exec" {
    command =  "echo \"\n[sftp_ext_ip]\n${var.sftp_server_ext_ip}\" >> ${var.client_name}.${var.environment}.endpoints"
  }

 #Create sftp DNS inventory
  provisioner "local-exec" {
    command =  "echo \"\n[sftp_dns]\n${var.sftp_server_public_dns}\" >> ${var.client_name}.${var.environment}.endpoints"
  }
 }
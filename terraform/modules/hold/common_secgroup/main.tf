# Variable inputs to module

variable "vpc_cidr" {
  description = "Subnet of VPC"
}

variable "vpc_id" {
  description = "The interpolated VPC ID"
}

variable "environment" {
  description = "Environment name"
}

variable "client_name" {
  description = "Client name"
}

variable "office_subnet_range" {
  description = "The subnets from the main office."
}

variable "security_group_bastion" {
  description = "The bastion SG id"
}

variable "security_group_web" {
  description = "The web SG id"
}

variable "security_group_rds" {
  description = "The web SG id"
}


/* Setup common security group */
resource "aws_security_group" "client_name_common_security_group" {
  name        = "${var.client_name}_${var.environment}_common_security_group"
  description = "Common ports"
  vpc_id      = "${var.vpc_id}"

  ingress {
    from_port   = "0"
    to_port     = "0"
    protocol    = "-1"
    cidr_blocks = ["${var.vpc_cidr}"]
  }

  egress {
    from_port   = "0"
    to_port     = "0"
    protocol    = "-1"
    cidr_blocks = ["${var.vpc_cidr}"]
  }

  tags {
    Name = "${var.client_name}-${var.environment}-common-security-group"
    Terraform  = "true"    
  }
}

/* Security group for bastion instance for inbound SSH */
resource "aws_security_group" "client_name_bastion_security_group" {
  name        = "${var.client_name}_${var.environment}_bastion_security_group"
  description = "Allow SSH from office IP range"
  vpc_id      = "${var.vpc_id}"

  ingress {
    from_port   = "22"
    to_port     = "22"
    protocol    = "tcp"
    cidr_blocks = "${split(",",var.office_subnet_range)}"
  }

  ingress {
    from_port   = "22"
    to_port     = "22"
    protocol    = "tcp"
    cidr_blocks = ["${var.vpc_cidr}"]
  }

  egress {
    from_port   = "80"
    to_port     = "80"
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  #  egress {
  #   from_port       = "22"
  #   to_port         = "22"
  #   protocol        = "tcp"
  #   security_groups = ["${var.security_group_rds}"]
  # }

   egress {
    from_port       = "22"
    to_port         = "22"
    protocol        = "tcp"
    security_groups = ["${var.security_group_web}"]
  }

   egress {
    from_port       = "22"
    to_port         = "22"
    protocol        = "tcp"
    cidr_blocks     = ["${var.vpc_cidr}"]
  }

   egress {
    from_port       = "443"
    to_port         = "443"
    protocol        = "tcp"
    cidr_blocks     = ["0.0.0.0/0"]
  }

  tags {
    Name = "${var.client_name}-${var.environment}-bastion-security-group"
    Terraform  = "true"    
  }
}

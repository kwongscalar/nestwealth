output "common_security_group_id" {
  value = "${aws_security_group.client_name_common_security_group.id}"
}

output "bastion_security_group_id" {
  value = "${aws_security_group.client_name_bastion_security_group.id}"
}
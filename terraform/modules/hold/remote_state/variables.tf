variable "client_name" {
  description = "The client name"
}

variable "environment" {
  description = "The environment"
}

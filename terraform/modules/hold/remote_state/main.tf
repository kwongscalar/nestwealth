# Create S3 bucket to hold remote state file

resource "aws_s3_bucket" "remote_state_bucket" {
  bucket = "${var.client_name}-remote_state-${var.environment}"
  acl    = "authenticated-read"
  
  versioning {
    enabled = true
  }

  tags {
    Name = "${var.client_name}-remote_state-${var.environment}"
    Environment = "${var.environment}"
  }
}

/* Security group for ALB  */
resource "aws_security_group" "alb_sg" {
  name        = "client_name_alb_security_group"
  description = "Load balancer ingress ports"
  vpc_id      = "${var.vpc_id}"

  ingress {
    from_port   = "80"
    to_port     = "80"
    protocol    = "tcp"
    cidr_blocks = ["${var.alb_inbound_ip_range}"]
  }

  ingress {
    from_port   = "443"
    to_port     = "443"
    protocol    = "tcp"
    cidr_blocks = ["${var.alb_inbound_ip_range}"]
  }

  egress {
    from_port   = "0"
    to_port     = "0"
    protocol    = "-1"
    cidr_blocks = ["${var.vpc_subnet_range}"]
  }

  tags {
    Name = "${var.client_name}-${var.environment}-alb-security-group"
  }
}

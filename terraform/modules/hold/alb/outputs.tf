output "alb_target_group80_arns" {
  value = ["${aws_alb_target_group.client_name_alb80_tg.arn}"]
}

output "alb_target_group9000_arns" {
  value = ["${aws_alb_target_group.client_name_alb9000_tg.arn}"]
}

output "alb_public_dns" {
  value = "${aws_alb.client_name_alb.public_dns}"
}

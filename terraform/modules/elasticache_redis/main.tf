variable "client_name" {}
variable "environment" {}
variable "instance_type" { default = "cache.t2.small" }
variable "subnet_id" {}
variable "elasticache_engine" { default = "redis" }
variable "elasticache_port" { default = "6379" }
variable "num_cache_nodes" { default = "1" }
variable "parameter_group_name" { default = "default.redis3.2" }
variable "security_groups" {}


# Create a Redis subnet group
resource "aws_elasticache_subnet_group" "client_name_redis_subnet_group" {
  name            = "${var.client_name}-${var.environment}-redis-subnet-group"
  subnet_ids      = ["${split(",", var.subnet_id)}"]
}

resource "aws_elasticache_cluster" "client_name_elasticache" {
    cluster_id           = "${var.client_name}-redis${var.environment}"
    engine               = "${var.elasticache_engine}"
    node_type            = "${var.instance_type}"
    port                 = "${var.elasticache_port}"
    num_cache_nodes      = "${var.num_cache_nodes}"
    parameter_group_name = "${var.parameter_group_name}"
    subnet_group_name    = "${aws_elasticache_subnet_group.client_name_redis_subnet_group.name}"
    security_group_ids   = ["${split(",", var.security_groups)}"]


  tags {
    Name        = "${var.client_name}-redis-${var.environment}"
    Terraform   = "true"
    Environment = "${var.environment}"
  }
}
output "redis_hostname" {
  value = "${aws_elasticache_cluster.client_name_elasticache.cache_nodes.0.address}"
}

output "redis_port" {
  value = "${aws_elasticache_cluster.client_name_elasticache.cache_nodes.0.port}"
}

# output "redis_endpoint" {
#   value = "${join(":", aws_elasticache_cluster.client_name_elasticache.cache_nodes.0.address, aws_elasticache_cluster.client_name_elasticache.cache_nodes.0.port)}"
# }
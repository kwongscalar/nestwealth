variable "vpc_id" {
  description = "The interpolated VPC ID"
}

variable "environment" {
  description = "Environment name"
}

variable "client_name" {
  description = "Client name"
}

variable "security_group_bastion" {
  description = "The bastion SG id"
}

variable "security_group_web" {
  description = "The web SG id"
}

variable "security_group_rds" {
  description = "The web SG id"
}

/* Security group for web instances */
resource "aws_security_group" "client_name_api_security_group" {
  name        = "${var.client_name}_${var.environment}_api_security_group"
  description = "Allow access to node ports"
  vpc_id      = "${var.vpc_id}"
 
   ingress {
    from_port       = 0
    to_port         = 0
    protocol        = "-1"
    security_groups = []
    self            = true
  }

  ingress {
    from_port       = 22
    to_port         = 22
    protocol        = "tcp"
    security_groups = ["${var.security_group_bastion}"]
    self            = false
  }

  ingress {
    from_port       = 3080
    to_port         = 3080
    protocol        = "tcp"
    security_groups = ["${var.security_group_web}"]
    self            = false
  }

  egress {
    from_port       = 80
    to_port         = 80
    protocol        = "tcp"
    cidr_blocks     = ["0.0.0.0/0"]
  }

  egress {
    from_port       = 0
    to_port         = 0
    protocol        = "-1"
    security_groups = []
    self            = true
  }

  egress {
    from_port       = 22
    to_port         = 22
    protocol        = "tcp"
    cidr_blocks     = ["192.30.252.0/22"]
  }

  egress {
    from_port       = 465
    to_port         = 465
    protocol        = "tcp"
    cidr_blocks     = ["0.0.0.0/0"]
  }

  egress {
    from_port       = 27017
    to_port         = 27017
    protocol        = "tcp"
    security_groups = ["${var.security_group_rds}"]
    self            = false
  }

  egress {
    from_port       = 3306
    to_port         = 3306
    protocol        = "tcp"
    security_groups = ["${var.security_group_rds}"]
    self            = false
  }

  egress {
    from_port       = 443
    to_port         = 443
    protocol        = "tcp"
    cidr_blocks     = ["0.0.0.0/0"]
  }

  egress {
    from_port       = 9418
    to_port         = 9418
    protocol        = "tcp"
    cidr_blocks     = ["192.30.252.0/22"]
  }

  tags {
    Name = "${var.client_name}-${var.environment}-api-security-group"
    Terraform  = "true"
  }
}


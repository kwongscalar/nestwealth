output "vpc_id" {
  value = "${aws_vpc.client_name_vpc.id}"
}

output "private_vpc_subnet_range_app_az1_id" {
  value = "${aws_subnet.client_name_private_subnet_app_az1.id}"
}

output "private_vpc_subnet_range_app_az2_id" {
  value = "${aws_subnet.client_name_private_subnet_app_az2.id}"
}

output "private_vpc_subnet_range_db_az1_id" {
  value = "${aws_subnet.client_name_private_subnet_db_az1.id}"
}

output "private_vpc_subnet_range_db_az2_id" {
  value = "${aws_subnet.client_name_private_subnet_db_az2.id}"
}

output "public_vpc_subnet_range_az1_id" {
  value = "${aws_subnet.client_name_public_subnet_az1.id}"
}

output "public_vpc_subnet_range_az2_id" {
  value = "${aws_subnet.client_name_public_subnet_az2.id}"
}

# Variable inputs to VPC module

#variable "vpc_id" {
#  description = "The ID of the VPC"
#}

# variable "region" {
#   description = "AWS region"
#   default     = "us-east-1"
# }

variable "vpc_cidr" {
  description = "The subnet range of the VPC"
}

variable "environment" {
  description = "Environment name"
}

variable "client_name" {
  description = "Client name"
}

variable "enable_dns_hostnames" {
  description = "Should be true if you want to use private DNS within the VPC"
  default     = true
}

variable "enable_dns_support" {
  description = "Should be true if you want to use private DNS within the VPC"
  default     = true
}

variable "public_vpc_subnet_range_az1" {
  description = "The public subnet range for AZ1"
}

variable "public_vpc_subnet_range_az2" {
  description = "The public subnet range for AZ2"
}

variable "private_vpc_subnet_range_app_az1" {
  description = "The public subnet range for apps in AZ1"
}

variable "private_vpc_subnet_range_app_az2" {
  description = "The public subnet range for apps in AZ2"
}

variable "private_vpc_subnet_range_db_az1" {
  description = "The public subnet range for DB in AZ1"
}

variable "private_vpc_subnet_range_db_az2" {
  description = "The public subnet range for DB in AZ2"
}

variable "office_subnet_range" {
  description = "The external NAT(s) for your office"
}

variable "open_subnet_range" {
  description = "All IPs in a subnet"
  default = "0.0.0.0/0"
}


/* This Terraform file creates a VPC, public and private 
subnets in two AZs, route tables, and managed NAT gateways */

# Create the VPC 
resource "aws_vpc" "client_name_vpc" {
  cidr_block           = "${var.vpc_cidr}"
  enable_dns_hostnames = "${var.enable_dns_hostnames}"

  tags {
    Name = "${var.client_name}-${var.environment}-vpc"
    Terraform  = "true"    
  }
}

# Setup the Internet gateway
resource "aws_internet_gateway" "client_name_igw" {
  vpc_id = "${aws_vpc.client_name_vpc.id}"

  tags {
    Name = "${var.client_name}-${var.environment}-igw"
    Terraform  = "true"    
  }
}

# Create public routing table
resource "aws_route_table" "client_name_public_route_table_az1" {
  vpc_id = "${aws_vpc.client_name_vpc.id}"
  route {
    cidr_block = "${var.open_subnet_range}"
    gateway_id = "${aws_internet_gateway.client_name_igw.id}"
  }

  tags {
    Name = "${var.client_name}-${var.environment}-public-rt-az1"
    Terraform  = "true"    
  }
}

# Create public routing table for AZ2
resource "aws_route_table" "client_name_public_route_table_az2" {
 vpc_id = "${aws_vpc.client_name_vpc.id}"
 route {
   cidr_block = "${var.open_subnet_range}"
   gateway_id = "${aws_internet_gateway.client_name_igw.id}"
 }

 tags {
   Name = "${var.client_name}-${var.environment}-public-rt-az2"
   Terraform  = "true"   
 }
}

# Create private routing table for app subnet in AZ1
resource "aws_route_table" "client_name_private_route_table_app_az1" {
  vpc_id = "${aws_vpc.client_name_vpc.id}"
  route {
    cidr_block = "${var.open_subnet_range}"
    nat_gateway_id = "${aws_nat_gateway.client_name_natgw_az1.id}"
  }

  tags {
    Name = "${var.client_name}-${var.environment}-private-rt-app-az1"
    Terraform  = "true"    
  }
}


# Create private routing table for app subnet in AZ2
resource "aws_route_table" "client_name_private_route_table_app_az2" {
  vpc_id = "${aws_vpc.client_name_vpc.id}"
  route {
    cidr_block = "${var.open_subnet_range}"
    nat_gateway_id = "${aws_nat_gateway.client_name_natgw_az2.id}"
  }

  tags {
    Name = "${var.client_name}-${var.environment}-private-rt-app-az2"
    Terraform  = "true"    
  }
}

# Create private routing table for DB subnet in AZ1
resource "aws_route_table" "client_name_private_route_table_db_az1" {
  vpc_id = "${aws_vpc.client_name_vpc.id}"
  route {
    cidr_block = "${var.open_subnet_range}"
    nat_gateway_id = "${aws_nat_gateway.client_name_natgw_az1.id}"
  }

  tags {
    Name = "${var.client_name}-${var.environment}-private-rt-db-az1"
    Terraform  = "true"    
  }
}

# Create private routing table for DB subnet in AZ2
resource "aws_route_table" "client_name_private_route_table_db_az2" {
  vpc_id = "${aws_vpc.client_name_vpc.id}"
  route {
    cidr_block = "${var.open_subnet_range}"
    nat_gateway_id = "${aws_nat_gateway.client_name_natgw_az2.id}"
  }

  tags {
    Name = "${var.client_name}-${var.environment}-private-rt-db-az2"
    Terraform  = "true"    
  }
}

# Create EIP for NAT gateway in AZ1
resource "aws_eip" "client_name_eip_az1" {
  vpc = true
}

# Create EIP for NAT gateway in AZ2
resource "aws_eip" "client_name_eip_az2" {
  vpc = true
}

# Create NAT gateway in AZ1
resource "aws_nat_gateway" "client_name_natgw_az1" {
  allocation_id = "${aws_eip.client_name_eip_az1.id}"
  subnet_id     = "${aws_subnet.client_name_public_subnet_az1.id}"
  depends_on    = ["aws_internet_gateway.client_name_igw"]
}

# Create NAT gateway in AZ2
resource "aws_nat_gateway" "client_name_natgw_az2" {
  allocation_id = "${aws_eip.client_name_eip_az2.id}"
  subnet_id     = "${aws_subnet.client_name_public_subnet_az2.id}"
  depends_on    = ["aws_internet_gateway.client_name_igw"]
}

/* Get the list of AZs in the defined region */
# Declare data source
data "aws_availability_zones" "available" {}

/* Create the public and private subnets in multiple AZs*/

# Public subnet in AZ1
resource "aws_subnet" "client_name_public_subnet_az1" {
  vpc_id            = "${aws_vpc.client_name_vpc.id}"
  cidr_block        = "${var.public_vpc_subnet_range_az1}"
  availability_zone = "${data.aws_availability_zones.available.names[0]}"

  tags {
    Name = "${var.client_name}-${var.environment}-public-subnet-az1"
    Terraform  = "true"
  }
}

# Associate route table for public subnet in AZ1
resource "aws_route_table_association" "client_name_public_route_az1" {
  subnet_id      = "${aws_subnet.client_name_public_subnet_az1.id}"
  route_table_id = "${aws_route_table.client_name_public_route_table_az1.id}"
}

# Public subnet in AZ2
resource "aws_subnet" "client_name_public_subnet_az2" {
  vpc_id            = "${aws_vpc.client_name_vpc.id}"
  cidr_block        = "${var.public_vpc_subnet_range_az2}"
  availability_zone = "${data.aws_availability_zones.available.names[1]}"

  tags {
    Name = "${var.client_name}-${var.environment}-public-subnet-az2"
    Terraform  = "true"
  }
}

# Associate route table for public subnet in AZ2
resource "aws_route_table_association" "client_name_public_route_az2" {
  subnet_id      = "${aws_subnet.client_name_public_subnet_az2.id}"
  route_table_id = "${aws_route_table.client_name_public_route_table_az2.id}"
}

# Private app subnet in AZ1
resource "aws_subnet" "client_name_private_subnet_app_az1" {
  vpc_id            = "${aws_vpc.client_name_vpc.id}"
  cidr_block        = "${var.private_vpc_subnet_range_app_az1}"
  availability_zone = "${data.aws_availability_zones.available.names[0]}"

  tags {
    Name = "${var.client_name}-${var.environment}-private-subnet-app-az1"
    Terraform  = "true"
  }
}

# Associate route table for private app subnet in AZ1
resource "aws_route_table_association" "client_name_private_route_app_az1" {
  subnet_id      = "${aws_subnet.client_name_private_subnet_app_az1.id}"
  route_table_id = "${aws_route_table.client_name_private_route_table_app_az1.id}"
}

# Private app subnet in AZ2
resource "aws_subnet" "client_name_private_subnet_app_az2" {
  vpc_id            = "${aws_vpc.client_name_vpc.id}"
  cidr_block        = "${var.private_vpc_subnet_range_app_az2}"
  availability_zone = "${data.aws_availability_zones.available.names[1]}"

  tags {
    Name = "${var.client_name}-${var.environment}-private-subnet-app-az2"
    Terraform  = "true"
  }
}

# Associate route table for private app subnet in AZ2
resource "aws_route_table_association" "client_name_private_route_app_az2" {
  subnet_id      = "${aws_subnet.client_name_private_subnet_app_az2.id}"
  route_table_id = "${aws_route_table.client_name_private_route_table_app_az2.id}"
}

# Private db subnet in AZ1
resource "aws_subnet" "client_name_private_subnet_db_az1" {
  vpc_id            = "${aws_vpc.client_name_vpc.id}"
  cidr_block        = "${var.private_vpc_subnet_range_db_az1}"
  availability_zone = "${data.aws_availability_zones.available.names[0]}"

  tags {
    Name = "${var.client_name}-${var.environment}-private-subnet-db-az1"
    Terraform  = "true"
  }
}

# Associate route table for private db subnet in AZ1
resource "aws_route_table_association" "client_name_private_route_db_az1" {
  subnet_id      = "${aws_subnet.client_name_private_subnet_db_az1.id}"
  route_table_id = "${aws_route_table.client_name_private_route_table_db_az1.id}"
}

# Private app subnet in AZ2
resource "aws_subnet" "client_name_private_subnet_db_az2" {
  vpc_id            = "${aws_vpc.client_name_vpc.id}"
  cidr_block        = "${var.private_vpc_subnet_range_db_az2}"
  availability_zone = "${data.aws_availability_zones.available.names[1]}"

  tags {
    Name = "${var.client_name}-${var.environment}-private-subnet-db-az2"
    Terraform  = "true"
  }
}

# Associate route table for private app subnet in AZ2
resource "aws_route_table_association" "client_name_private_route_db_az2" {
  subnet_id      = "${aws_subnet.client_name_private_subnet_db_az2.id}"
  route_table_id = "${aws_route_table.client_name_private_route_table_db_az2.id}"
}
# Variables for ELB
variable "client_name" {}
variable "elb_subnets" {}
variable "vpc_cidr" {}
variable "environment" {}
variable "security_groups" {}
variable "server_role" { default = "nodejs" }
variable "internal" { default = "false" }
variable "instance_port" {}
variable "instance_protocol" {}
variable "lb_port" {}
variable "lb_port2" {}
variable "ssl_certificate_domain" {}
variable "lb_protocol" {}
variable "lb_protocol2" {}
variable "health_check_healthy_threshold" { default = "5" }
variable "health_check_unhealthy_threshold" { default = "7" }
variable "health_check_target" {}
variable "health_check_interval" {}
variable "health_check_timeout" {}

# Variables for launch configuration
variable "instance_type" { default = "t2.small" }
variable "key_name" {}
variable "ebs_optimized" { default = "false" }
variable "enable_monitoring" { default = "false" }
variable "associate_public_ip" { default = "false" }
variable "root_volume_type" { default = "gp2" }
variable "root_volume_size" { default = "30" }
variable "ami_filter_value" { default = "prod_base_*" }
variable "ami_owner" { default = "841968689972" }
variable "user_data_script" { default = "user_data.sh"}

# Variables for autoscaling group
variable "asg_subnets" {}
variable "asg_max_size" {}
variable "asg_min_size" {}
variable "asg_health_check_grace_period" { default = "300" }
variable "asg_health_check_type" { default = "ELB" }
variable "asg_desired_capacity" {}
variable "asg_termination_policy" { default = "OldestInstance"}

# Find the SSL certificate
data "aws_acm_certificate" "client_name_ssl_cert" {
  domain = "${var.ssl_certificate_domain}"
  statuses = ["ISSUED"]
}

# Create an external ELB
resource "aws_elb" "client_name_elb" {
  name            = "${var.client_name}-${var.environment}-${var.server_role}"
  subnets         = ["${split(",", var.elb_subnets)}"]
  security_groups = ["${split(",", var.security_groups)}"]
  internal        = "${var.internal}"

  listener {
    instance_port     = "${var.instance_port}"
    instance_protocol = "${var.instance_protocol}"
    lb_port           = "${var.lb_port}"
    lb_protocol       = "${var.lb_protocol}"
  }

  listener {
    instance_port      = "${var.instance_port}"
    instance_protocol  = "${var.instance_protocol}"
    lb_port            = "${var.lb_port2}"
    lb_protocol        = "${var.lb_protocol2}"
    ssl_certificate_id = "${data.aws_acm_certificate.client_name_ssl_cert.arn}"
  }

  health_check {
    healthy_threshold   = "${var.health_check_healthy_threshold}"
    unhealthy_threshold = "${var.health_check_unhealthy_threshold}"
    timeout             = "${var.health_check_timeout}"
    target              = "${var.health_check_target}"
    interval            = "${var.health_check_interval}"
  }

  tags {
    Name = "${var.client_name}-${var.environment}-${var.server_role}-elb"
    Role = "${var.server_role}"
    Environment = "${var.environment}"
    Terraform  = "true"
  }
}


# Find the custom AMI by region
data "aws_ami" "custom_nodejs_ami" {
  most_recent = true

  filter {
    name   = "name"
    #values = ["uat_base_*"]
    values = ["${var.ami_filter_value}"]

  }

  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }

  # Owned by Nest Wealth
  owners = ["${var.ami_owner}"]
}

# Setup launch configuration
resource "aws_launch_configuration" "client_name_launch_config" {
  name_prefix                 = "${var.client_name}_${var.environment}_launch_config_${var.server_role}-"
  image_id                    = "${data.aws_ami.custom_nodejs_ami.id}"
  instance_type               = "${var.instance_type}"
  key_name                    = "${var.key_name}"
  iam_instance_profile        = "${var.client_name}_generic_instance_profile"
  security_groups             = ["${split(",", var.security_groups)}"]
#  user_data                   = "${file("user_data.sh")}"
  user_data                   = "${file(var.user_data_script)}"
  ebs_optimized               = "${var.ebs_optimized}"
  enable_monitoring           = "false"
  associate_public_ip_address = "${var.associate_public_ip}"

  root_block_device {
    volume_type = "${var.root_volume_type}"
    volume_size = "${var.root_volume_size}"
  }

  lifecycle {
    create_before_destroy = true
  }
}

# Setup scaling group
resource "aws_autoscaling_group" "client_name_autoscaling_group" {
  name                      = "${var.client_name}_${var.environment}_asg_${var.server_role}"
  max_size                  = "${var.asg_max_size}"
  min_size                  = "${var.asg_min_size}"
  health_check_grace_period = "${var.asg_health_check_grace_period}"
  health_check_type         = "${var.asg_health_check_type}"
  desired_capacity          = "${var.asg_desired_capacity}"
  termination_policies      = ["${var.asg_termination_policy}"]
  launch_configuration      = "${aws_launch_configuration.client_name_launch_config.name}"
  vpc_zone_identifier       = ["${split(",", var.asg_subnets)}"]
  load_balancers            = ["${aws_elb.client_name_elb.name}"]


  tag {
    key                 = "Name"
    value               = "${var.client_name}-${var.environment}-asg-${var.server_role}"
    propagate_at_launch = true
  }

  tag {
    key                 = "Terraform"
    value               = "true"
    propagate_at_launch = true
  }  
}

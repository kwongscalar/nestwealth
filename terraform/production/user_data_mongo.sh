#!/bin/bash

export PATH="/usr/bin:/usr/local/bin:/sbin:/usr/sbin:${PATH}"

echo "Hi from Terraform" >> /tmp/terraformhi.txt

pip install ansible

# Wait for EBS volumes to be attached
while [ ! -e /dev/xvdf ]; do 
  echo "waiting for /dev/xvdf volume to attach"
  sleep 5
done
echo "/dev/xvdf volume is attached"

while [ ! -e /dev/xvdg ]; do 
  echo "waiting for /dev/xvdg volume to attach"
  sleep 5
done
echo "/dev/xvdg volume is attached"

while [ ! -e /dev/xvdh ]; do 
  echo "waiting for /dev/xvdh volume to attach"
  sleep 5
done
echo "/dev/xvdh volume is attached"


# Create the filesystem on the EBS volumes
VOLUMES="xvdf xvdg xvdh"

for i in $VOLUMES
do
  file -s /dev/$i
  mkfs -t ext4 /dev/$i
done

# Make mount points permanent in /etc/fstab
echo "/dev/xvdf /data ext4 defaults,nofail 0 2" >> /etc/fstab
echo "/dev/xvdg /journal ext4 defaults,nofail 0 2" >> /etc/fstab
echo "/dev/xvdh /logs ext4 defaults,nofail 0 2" >> /etc/fstab

# Create the mount point sub-dirs and mount the volumes
MOUNTS="data journal logs"

for j in $MOUNTS
do
  mkdir $j
  mount /$j
done

# Grab Ansible playbooks from S3
FILENAME="nestwealth-ansible.tar.gz"
ANSIBLE_ROOT_DIR="/etc/ansible"
mkdir -p ${ANSIBLE_ROOT_DIR}
cd ${ANSIBLE_ROOT_DIR}
aws s3 cp s3://nestwealth-ansible/${FILENAME} ${ANSIBLE_ROOT_DIR} --region=ca-central-1
tar -zxvf ${FILENAME}
ansible-playbook -i localhost, -c local mongodb.yml

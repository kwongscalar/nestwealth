/* This file is for variable overrides.  
Variables here will override all variables in main.tf
*/

#region                 = "us-east-1"
#instance_type          = "t2.small"
#ssl_certificate_domain = "scalar.kitchen"
#key_name               = "kenwong-kp"
#key_name               = "ken-alan-test-key"
health_check_target    = "TCP:22"

# # Amazon Linux in us-east-1
#ami_filter_value       = "amzn-ami-hvm-*x86_64-gp2"
#ami_owner              = "137112412989"

# # Redhat Linux in us-east-1
# ami_filter_value = "RHEL-7.3_HVM_GA-*-x86_64-1-Hourly2-GP2"
# ami_owner = "309956199498"
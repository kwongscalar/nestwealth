#!/bin/bash

REGION="ca-central-1"
echo "Hi from Terraform" >> /tmp/terraformhi.txt

yum install -y git awslogs
pip install ansible
cd ~ec2-user
aws s3 cp s3://nestwealth-ansible/id_rsa .ssh --region=${REGION}
aws s3 cp s3://nestwealth-ansible/known_hosts .ssh --region=${REGION}
chmod 700 .ssh/{id_rsa,known_hosts}
chown ec2-user:ec2-user .ssh/{id_rsa,known_hosts}
sudo su - ec2-user -c "git clone git@github.com:nestwealth/join_webserver.git"

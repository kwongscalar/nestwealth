#Define variables

variable "environment" {
  description = "The environment - dev, uat, prod, etc."
  default     = "prod"
}

variable "region" {
  description = "AWS region"
  default     = "ca-central-1"
}

variable "client_name" {
  description = "The name of the client.  Used for tagging and namespacing."
  default     = "nestwealth"
}

variable "vpc_cidr" {
  description = "The CIDR of the VPC"
  default     = "10.21.0.0/16"
}

variable "public_vpc_subnet_range_az1" {
  description = "The public subnet range for AZ1"
  default     = "10.21.1.0/24"
}

variable "public_vpc_subnet_range_az2" {
  description = "The public subnet range for AZ2"
  default     = "10.21.2.0/24"
}

variable "private_vpc_subnet_range_app_az1" {
  description = "The public subnet range for AZ1"
  default     = "10.21.10.0/24"
}

variable "private_vpc_subnet_range_app_az2" {
  description = "The public subnet range for AZ2"
  default     = "10.21.20.0/24"
}

variable "private_vpc_subnet_range_db_az1" {
  description = "The public subnet range for AZ1"
  default     = "10.21.30.0/24"  
}

variable "private_vpc_subnet_range_db_az2" {
  description = "The public subnet range for AZ2"
  default     = "10.21.40.0/24"
}

variable "office_subnet_range" {
  description = "The external NAT(s) for your office"
  default     = "74.213.172.118/32,135.23.139.72/32,70.52.243.131/32,74.12.32.33/32,99.235.172.246/32,173.33.228.185/32"
}

variable "ips_allowed_to_elb" {
  description = "The external NAT(s) allowed to the ELB"
  default     = "70.52.243.131/32,209.67.98.0/24,206.25.247.0/24,209.46.117.0/24,162.248.184.0/24,54.149.21.90/32,54.69.114.54/32,52.25.122.31/32,52.25.145.215/32,52.26.192.160/32,52.24.91.157/32,52.27.126.9/32,52.11.152.229/32,74.12.32.33/32,70.30.13.184/32,207.102.98.0/26,66.119.189.73/32,158.85.82.180/31,99.242.192.54/32,135.23.139.72/32,76.64.206.189/32,69.17.128.132/30,104.251.96.160/29,108.174.29.160/29"
}

variable "key_name" {
  description = "The keypair to use"
  default     = "can_prod"
}

variable "ssl_certificate_domain" {
  description = "The domain of the SSL certifcate to use"
  default     = "*.nestwealth.com"
}

variable "instance_type" {
  description = "The instance type for production"
  default     = "t2.medium"
}

variable "instance_profile_name" {
  description = "The instance type for production"
  default     = "web_instance_profile"
}

variable "web_instance_profile_name" {
  description = "The instance type for production"
  default     = "web_instance_profile"
}

variable "instance_port" {
  description = "The port on the instance to route to"
  default     = "3080"
}

variable "instance_protocol" {
  description = "The protocol to use for the port.  HTTP, HTTPS, TCP or SSL only."
  default     = "http"
}

variable "lb_port" {
  description = "The port to listen on for the load balancer"
  default     = "80"
}

variable "lb_protocol" {
  description = "The protocol to listen on.  HTTP, HTTPS, TCP or SSL only."
  default     = "http"
}

variable "lb_port2" {
  description = "The secondary port to listen on for the load balancer"
  default     = "443"
}

variable "lb_protocol2" {
  description = "The secondary protocol to listen on.  HTTP, HTTPS, TCP or SSL only."
  default     = "https"
}

variable "health_check_healthy_threshold" {
  description = "The number of checks before the instance is declared healthy."
  default     = "5"
}

variable "health_check_unhealthy_threshold" {
  description = "The number of failed checks before the instance is declared unhealthy."
  default     = "7"
}

variable "health_check_target" {
  description = "The actual health check. eg. HTTP:80/test"
  default     = "HTTP:3080/elb_health_check"
}

variable "health_check_interval" {
  description = "The interval between checks."
  default     = "30"
}

variable "health_check_timeout" {
  description = "The timeout before the check fails."
  default     = "15"
}

variable "asg_max_size" {
  description = "The maximum size of the auto scale group."
  default     = "1"
}

variable "asg_min_size" {
    description = "The minimum size of the auto scale group."
    default     = "1"
}

variable "asg_health_check_grace_period" {
  description = "Time in seconds, after instance is in service, before starting to check health."
  default     = "300"
}

variable "asg_health_check_type" {
  description = "The type of health check.  Only ELB or EC2 are valid."
  default     = "ELB"
}

variable "asg_desired_capacity" {
  description = "The number of EC2 instances that should be running in the autoscaling group"
  default     = "1"
}

variable "ami_filter_value" { 
  description = "The regex to find the AMI"
  default     = "prod_base_*"
}

variable "ami_owner" { 
  default = "841968689972" 
  description = "The owner ID of the AMI"
}

variable "mongo_data_disk_size" {
  default = "100"  
  description = "The size of the data volume."   
}

variable "mongo_journal_disk_size" {
  default = "30"  
  description = "The size of the journal volume."   
}

variable "mongo_logs_disk_size" {
  default = "30"  
  description = "The size of the logs volume."   
}

variable "cloudtrail_s3_bucket_name" {
  description = "The name of the Cloudtrail S3 bucket."
  default     = "cloudtrail-logs" 
}

# variable "autoscale_start_time" {
#   description = "The autoscale start time"
#   default     = "2017-04-01T15:00:00Z"
# }

# variable "autoscale_end_time" {
#   description = "The autoscale end time"
#   default     = "2017-04-02T15:45:00Z"
# }


provider "aws" {
  region = "${var.region}"
}

module "vpc" {
  source                           = "../modules/vpc"
  client_name                      = "${var.client_name}"
  vpc_cidr                         = "${var.vpc_cidr}"
  environment                      = "${var.environment}"
  office_subnet_range              = "${var.office_subnet_range}"
  public_vpc_subnet_range_az1      = "${var.public_vpc_subnet_range_az1}"
  public_vpc_subnet_range_az2      = "${var.public_vpc_subnet_range_az2}"
  private_vpc_subnet_range_app_az1 = "${var.private_vpc_subnet_range_app_az1}"
  private_vpc_subnet_range_app_az2 = "${var.private_vpc_subnet_range_app_az2}"
  private_vpc_subnet_range_db_az1  = "${var.private_vpc_subnet_range_db_az1}"
  private_vpc_subnet_range_db_az2  = "${var.private_vpc_subnet_range_db_az2}"
}

# Setup VPC flow logs Cloudwatch log group
module "cloudwatch_log_group_vpc_flow_logs" {
  source                           = "../modules/cloudwatch_log_group"
  client_name                      = "${var.client_name}"
  environment                      = "${var.environment}"
  retention_in_days                = "1"
  cloudwatch_log_group_name        = "vpcflowlogs-group"
}

# Setup the actual VPC flow logs 
# NOTE: this module only works on the full VPC.  
# There are no provisions for flow logs on subnet id or eni id.
module "vpc_flow_logs" {
  source                         = "../modules/vpc_flow_logs"
  client_name                    = "${var.client_name}"
  environment                    = "${var.environment}"  
  vpc_id                         = "${module.vpc.vpc_id}"
  cloudwatch_log_group_name      = "${module.cloudwatch_log_group_vpc_flow_logs.cloudwatch_log_group_name}"
  iam_role_arn                   = "${aws_iam_role.client_name_generic_role.arn}"
}

# Create SG for bastion host
module "bastion_secgroup" {
  source                 = "../modules/bastion_secgroup"
  client_name            = "${var.client_name}"
  vpc_cidr               = "${var.vpc_cidr}"
  office_subnet_range    = "${var.office_subnet_range}"
  vpc_id                 = "${module.vpc.vpc_id}"
  environment            = "${var.environment}"
  security_group_web     = "${module.web_secgroup.web_security_group_id}"
  security_group_rds     = "${module.rds_secgroup.rds_security_group_id}"  
}

# Create SG for RDS & Mongo servers
module "rds_secgroup" {
  source                 = "../modules/rds_secgroup"
  client_name            = "${var.client_name}"
  vpc_id                 = "${module.vpc.vpc_id}"
  vpc_cidr               = "${var.vpc_cidr}"
  environment            = "${var.environment}"
  security_group_bastion = "${module.bastion_secgroup.bastion_security_group_id}"
  security_group_web     = "${module.web_secgroup.web_security_group_id}"
  security_group_api     = "${module.api_secgroup.api_security_group_id}"
  allowed_subnets        = "${var.private_vpc_subnet_range_app_az1},${var.private_vpc_subnet_range_app_az2}"
}

# Create SG for web servers
module "web_secgroup" {
  source      = "../modules/web_secgroup"
  client_name = "${var.client_name}"
  vpc_id      = "${module.vpc.vpc_id}"
  environment = "${var.environment}"
}

# Create SG for Elastic Load Balancers (ELB)
module "elb_secgroup" {
  source             = "../modules/elb_secgroup"
  client_name        = "${var.client_name}"
  vpc_id             = "${module.vpc.vpc_id}"
  environment        = "${var.environment}"
  ips_allowed_to_elb = "${var.ips_allowed_to_elb}"
}

# Create SG for API servers
module "api_secgroup" {
  source                 = "../modules/api_secgroup"
  client_name            = "${var.client_name}"
  vpc_id                 = "${module.vpc.vpc_id}"
  environment            = "${var.environment}"
  security_group_bastion = "${module.bastion_secgroup.bastion_security_group_id}"
  security_group_web     = "${module.web_secgroup.web_security_group_id}"
  security_group_rds     = "${module.rds_secgroup.rds_security_group_id}"
}

# Create the bastion host
module "bastion" {
  source                           = "../modules/bastion"
  client_name                      = "${var.client_name}"
  environment                      = "${var.environment}"
  subnet_id                        = "${module.vpc.public_vpc_subnet_range_az1_id}"
  # For more security groups, just separate with a comma. eg.
  #security_groups                  = "${module.web_secgroup.web_security_group_id},${module.bastion_secgroup.bastion_security_group_id}"
  security_groups                  = "${module.bastion_secgroup.bastion_security_group_id}"
  instance_type                    = "${var.instance_type}"
  server_role                      = "bastion"
  key_name                         = "${var.key_name}"
}

# Create the key server with an autoscaling group
module "key_server" {
  source                           = "../modules/no_elb_asg"
  client_name                      = "${var.client_name}"
  vpc_cidr                         = "${var.vpc_cidr}"
  asg_subnets                      = "${module.vpc.private_vpc_subnet_range_app_az1_id},${module.vpc.private_vpc_subnet_range_app_az2_id}"
  environment                      = "${var.environment}"
  server_role                      = "key-server"
  security_groups                  = "${module.api_secgroup.api_security_group_id},${module.bastion_secgroup.bastion_security_group_id}"
  instance_type                    = "${var.instance_type}"
  key_name                         = "${var.key_name}"
  asg_max_size                     = "1" 
  asg_min_size                     = "1"
  asg_desired_capacity             = "1"
  asg_health_check_grace_period    = "${var.asg_health_check_grace_period}"  
  ami_filter_value                 = "${var.ami_filter_value}"
  ami_owner                        = "${var.ami_owner}"
  user_data_script                 = "user_data_key.sh"
}

# Create the standalone SFTP server
module "sftp_server" {
  source                           = "../modules/servers"
  client_name                      = "${var.client_name}"
  subnet_id                        = "${module.vpc.public_vpc_subnet_range_az1_id},${module.vpc.public_vpc_subnet_range_az2_id}"
  environment                      = "${var.environment}"
  server_role                      = "sftp-server"
  security_groups                  = "${module.bastion_secgroup.bastion_security_group_id}"
  instance_type                    = "${var.instance_type}"
  key_name                         = "${var.key_name}"
  user_data_template               = "user_data_sftp_template"
  ansible_tar_file                 = "nestwealth-ansible.tar.gz"
  ansible_playbook                 = "sftp.yml"
  create_eip                       = true
}

# Create the join-web servers with an autoscaling group
module "join_web" {
  source                           = "../modules/elb_asg_external"
  client_name                      = "${var.client_name}"
  vpc_cidr                         = "${var.vpc_cidr}"
  elb_subnets                      = "${module.vpc.public_vpc_subnet_range_az1_id},${module.vpc.public_vpc_subnet_range_az2_id}"
  asg_subnets                      = "${module.vpc.private_vpc_subnet_range_app_az1_id},${module.vpc.private_vpc_subnet_range_app_az2_id}"
  environment                      = "${var.environment}"
  server_role                      = "join-web"
  security_groups                  = "${module.web_secgroup.web_security_group_id},${module.bastion_secgroup.bastion_security_group_id}"
  instance_type                    = "${var.instance_type}"
  key_name                         = "${var.key_name}"
  ssl_certificate_domain           = "${var.ssl_certificate_domain}"  
  lb_protocol                      = "http"
  lb_port                          = "80"
  lb_protocol2                     = "https"
  lb_port2                         = "443"
  instance_protocol                = "http"
  instance_port                    = "3080"
  #health_check_target              = "HTTP:3080/elb_health_check"
  health_check_target              = "${var.health_check_target}"
  health_check_timeout             = "10"
  health_check_interval            = "15"
  health_check_healthy_threshold   = "3"
  health_check_unhealthy_threshold = "5"
  asg_max_size                     = "4" 
  asg_min_size                     = "2"
  asg_desired_capacity             = "2"
  asg_health_check_grace_period    = "${var.asg_health_check_grace_period}"  
  ami_filter_value                 = "${var.ami_filter_value}"
  ami_owner                        = "${var.ami_owner}"
  user_data_script                 = "user_data_joinweb.sh"
}

# Create the join-api servers with an autoscaling group
module "join_api" {
  source                           = "../modules/elb_asg_internal"
  client_name                      = "${var.client_name}"
  vpc_cidr                         = "${var.vpc_cidr}"
  subnets                          = "${module.vpc.private_vpc_subnet_range_app_az1_id},${module.vpc.private_vpc_subnet_range_app_az2_id}"
  environment                      = "${var.environment}"
  server_role                      = "join-api"
  internal                         = "true"
  security_groups                  = "${module.api_secgroup.api_security_group_id},${module.bastion_secgroup.bastion_security_group_id}"
  instance_type                    = "${var.instance_type}"
  key_name                         = "${var.key_name}"
  lb_protocol                      = "http"
  lb_port                          = "3080"
  instance_protocol                = "http"
  instance_port                    = "3080"
  #health_check_target              = "HTTP:3080/elb_health_check"
  health_check_target              = "${var.health_check_target}"
  health_check_timeout             = "10"
  health_check_interval            = "15"
  health_check_healthy_threshold   = "3"
  health_check_unhealthy_threshold = "5"
  asg_max_size                     = "4" 
  asg_min_size                     = "2"
  asg_desired_capacity             = "2"
  asg_health_check_grace_period    = "${var.asg_health_check_grace_period}"
  ami_filter_value                 = "${var.ami_filter_value}"
  ami_owner                        = "${var.ami_owner}"
  user_data_script                 = "user_data_joinapi.sh"
}

# Create the port-web servers with an autoscaling group
module "port_web" {
  source                           = "../modules/elb_asg_external"
  client_name                      = "${var.client_name}"
  vpc_cidr                         = "${var.vpc_cidr}"
  elb_subnets                      = "${module.vpc.public_vpc_subnet_range_az1_id},${module.vpc.public_vpc_subnet_range_az2_id}"
  asg_subnets                      = "${module.vpc.private_vpc_subnet_range_app_az1_id},${module.vpc.private_vpc_subnet_range_app_az2_id}"
  environment                      = "${var.environment}"
  server_role                      = "port-web"
  security_groups                  = "${module.web_secgroup.web_security_group_id},${module.bastion_secgroup.bastion_security_group_id}"
  instance_type                    = "${var.instance_type}"
  key_name                         = "${var.key_name}"
  ssl_certificate_domain           = "${var.ssl_certificate_domain}"  
  lb_protocol                      = "http"
  lb_port                          = "80"
  lb_protocol2                     = "https"
  lb_port2                         = "443"
  instance_protocol                = "http"
  instance_port                    = "3080"
  health_check_target              = "${var.health_check_target}"
  health_check_timeout             = "10"
  health_check_interval            = "15"
  health_check_healthy_threshold   = "3"
  health_check_unhealthy_threshold = "5"
  asg_max_size                     = "4" 
  asg_min_size                     = "2"
  asg_desired_capacity             = "2"
  asg_health_check_grace_period    = "${var.asg_health_check_grace_period}"
  ami_filter_value                 = "${var.ami_filter_value}"
  ami_owner                        = "${var.ami_owner}"
  user_data_script                 = "user_data_portweb.sh"
}

# Create the port-api servers with an autoscaling group
module "port_api" {
  source                           = "../modules/elb_asg_internal"
  client_name                      = "${var.client_name}"
  vpc_cidr                         = "${var.vpc_cidr}"
  subnets                          = "${module.vpc.private_vpc_subnet_range_app_az1_id},${module.vpc.private_vpc_subnet_range_app_az2_id}"
  environment                      = "${var.environment}"
  server_role                      = "port-api"
  internal                         = "true"
  security_groups                  = "${module.api_secgroup.api_security_group_id},${module.bastion_secgroup.bastion_security_group_id}"
  instance_type                    = "${var.instance_type}"
  key_name                         = "${var.key_name}"
  lb_protocol                      = "http"
  lb_port                          = "3080"
  instance_protocol                = "http"
  instance_port                    = "3080"
  health_check_target              = "${var.health_check_target}"
  health_check_timeout             = "10"
  health_check_interval            = "15"
  health_check_healthy_threshold   = "3"
  health_check_unhealthy_threshold = "5"
  asg_max_size                     = "4" 
  asg_min_size                     = "2"
  asg_desired_capacity             = "2"
  asg_health_check_grace_period    = "${var.asg_health_check_grace_period}"
  ami_filter_value                 = "${var.ami_filter_value}"
  ami_owner                        = "${var.ami_owner}"  
  user_data_script                 = "user_data_portapi.sh"  
}

# Create the admin-web servers with an autoscaling group
module "admin_web" {
  source                           = "../modules/elb_asg_external"
  client_name                      = "${var.client_name}"
  vpc_cidr                         = "${var.vpc_cidr}"
  elb_subnets                      = "${module.vpc.public_vpc_subnet_range_az1_id},${module.vpc.public_vpc_subnet_range_az2_id}"
  asg_subnets                      = "${module.vpc.private_vpc_subnet_range_app_az1_id},${module.vpc.private_vpc_subnet_range_app_az2_id}"
  environment                      = "${var.environment}"
  server_role                      = "admin-web"
  security_groups                  = "${module.web_secgroup.web_security_group_id},${module.bastion_secgroup.bastion_security_group_id}"
  instance_type                    = "${var.instance_type}"
  key_name                         = "${var.key_name}"
  ssl_certificate_domain           = "${var.ssl_certificate_domain}"  
  lb_protocol                      = "http"
  lb_port                          = "80"
  instance_protocol                = "http"
  instance_port                    = "3080"
  lb_protocol2                     = "https"
  lb_port2                         = "443"
  health_check_target              = "${var.health_check_target}"
  health_check_timeout             = "10"
  health_check_interval            = "15"
  health_check_healthy_threshold   = "3"
  health_check_unhealthy_threshold = "5"
  asg_max_size                     = "2" 
  asg_min_size                     = "1"
  asg_desired_capacity             = "1"
  asg_health_check_grace_period    = "${var.asg_health_check_grace_period}"
  ami_filter_value                 = "${var.ami_filter_value}"
  ami_owner                        = "${var.ami_owner}"
  user_data_script                 = "user_data_adminweb.sh"
}

# Create the config-api servers with an autoscaling group
module "config_api" {
  source                           = "../modules/elb_asg_internal"
  client_name                      = "${var.client_name}"
  vpc_cidr                         = "${var.vpc_cidr}"
  subnets                          = "${module.vpc.private_vpc_subnet_range_app_az1_id},${module.vpc.private_vpc_subnet_range_app_az2_id}"
  environment                      = "${var.environment}"
  server_role                      = "config-api"
  internal                         = "true"
  security_groups                  = "${module.api_secgroup.api_security_group_id},${module.bastion_secgroup.bastion_security_group_id}"
  instance_type                    = "${var.instance_type}"
  key_name                         = "${var.key_name}"
  lb_protocol                      = "http"
  lb_port                          = "3080"
  instance_protocol                = "http"
  instance_port                    = "3080"
  health_check_target              = "${var.health_check_target}"
  health_check_timeout             = "10"
  health_check_interval            = "15"
  health_check_healthy_threshold   = "3"
  health_check_unhealthy_threshold = "5"
  asg_max_size                     = "4" 
  asg_min_size                     = "2"
  asg_desired_capacity             = "2"
  asg_health_check_grace_period    = "${var.asg_health_check_grace_period}"
  ami_filter_value                 = "${var.ami_filter_value}"
  ami_owner                        = "${var.ami_owner}"
}

# Create the svg-web servers with an autoscaling group
module "svg_web" {
  source                           = "../modules/elb_asg_external"
  client_name                      = "${var.client_name}"
  vpc_cidr                         = "${var.vpc_cidr}"
  elb_subnets                      = "${module.vpc.public_vpc_subnet_range_az1_id},${module.vpc.public_vpc_subnet_range_az2_id}"
  asg_subnets                      = "${module.vpc.private_vpc_subnet_range_app_az1_id},${module.vpc.private_vpc_subnet_range_app_az2_id}"
  environment                      = "${var.environment}"
  server_role                      = "svg-web"
  security_groups                  = "${module.web_secgroup.web_security_group_id},${module.bastion_secgroup.bastion_security_group_id}"
  instance_type                    = "${var.instance_type}"
  key_name                         = "${var.key_name}"
  ssl_certificate_domain           = "${var.ssl_certificate_domain}"  
  lb_protocol                      = "http"
  lb_port                          = "80"
  instance_protocol                = "http"
  instance_port                    = "3080"
  lb_protocol2                     = "https"
  lb_port2                         = "443"
  health_check_target              = "${var.health_check_target}"
  health_check_timeout             = "10"
  health_check_interval            = "15"
  health_check_healthy_threshold   = "3"
  health_check_unhealthy_threshold = "5"
  asg_max_size                     = "2" 
  asg_min_size                     = "2"
  asg_desired_capacity             = "2"
  asg_health_check_grace_period    = "${var.asg_health_check_grace_period}"
  ami_filter_value                 = "${var.ami_filter_value}"
  ami_owner                        = "${var.ami_owner}"
  user_data_script                 = "user_data_svgweb.sh"
}


# Create the svg-web servers with an autoscaling group
module "com_web" {
  source                           = "../modules/elb_asg_external"
  client_name                      = "${var.client_name}"
  vpc_cidr                         = "${var.vpc_cidr}"
  elb_subnets                      = "${module.vpc.public_vpc_subnet_range_az1_id},${module.vpc.public_vpc_subnet_range_az2_id}"
  asg_subnets                      = "${module.vpc.private_vpc_subnet_range_app_az1_id},${module.vpc.private_vpc_subnet_range_app_az2_id}"
  environment                      = "${var.environment}"
  server_role                      = "com-web"
  security_groups                  = "${module.web_secgroup.web_security_group_id},${module.bastion_secgroup.bastion_security_group_id}"
  instance_type                    = "${var.instance_type}"
  key_name                         = "${var.key_name}"
  ssl_certificate_domain           = "${var.ssl_certificate_domain}"  
  lb_protocol                      = "http"
  lb_port                          = "80"
  instance_protocol                = "http"
  instance_port                    = "3080"
  lb_protocol2                     = "https"
  lb_port2                         = "443"
  health_check_target              = "${var.health_check_target}"
  health_check_timeout             = "10"
  health_check_interval            = "15"
  health_check_healthy_threshold   = "3"
  health_check_unhealthy_threshold = "5"
  asg_max_size                     = "4" 
  asg_min_size                     = "2"
  asg_desired_capacity             = "2"
  asg_health_check_grace_period    = "${var.asg_health_check_grace_period}"
  ami_filter_value                 = "${var.ami_filter_value}"
  ami_owner                        = "${var.ami_owner}"
  user_data_script                 = "user_data_comweb.sh"
}

# Create the MongoDB master server
module "mongodb_master" {
  source                           = "../modules/mongodb"
  client_name                      = "${var.client_name}"
  environment                      = "${var.environment}"
  subnet_id                        = "${module.vpc.private_vpc_subnet_range_db_az2_id}"
  security_groups                  = "${module.rds_secgroup.rds_security_group_id},${module.bastion_secgroup.bastion_security_group_id}"
  instance_type                    = "${var.instance_type}"
  server_role                      = "mongodb"
  key_name                         = "${var.key_name}"
  mongo_data_disk_size             = "${var.mongo_data_disk_size}"
  mongo_journal_disk_size          = "${var.mongo_journal_disk_size}"
  mongo_logs_disk_size             = "${var.mongo_logs_disk_size}"
  user_data_script                 = "user_data_mongo.sh"
}

# Create the MongoDB replica server
module "mongodb_replica1" {
  source                           = "../modules/mongodb"
  client_name                      = "${var.client_name}"
  environment                      = "${var.environment}"
  subnet_id                        = "${module.vpc.private_vpc_subnet_range_db_az2_id}"
  security_groups                  = "${module.rds_secgroup.rds_security_group_id},${module.bastion_secgroup.bastion_security_group_id}"
  instance_type                    = "${var.instance_type}"
  server_role                      = "mongodb-rr"
  key_name                         = "${var.key_name}"
  mongo_data_disk_size             = "${var.mongo_data_disk_size}"
  mongo_journal_disk_size          = "${var.mongo_journal_disk_size}"
  mongo_logs_disk_size             = "${var.mongo_logs_disk_size}"
  user_data_script                 = "user_data_mongo.sh"
}

# Create the MongoDB replica server
module "mongodb_replica2" {
  source                           = "../modules/mongodb"
  client_name                      = "${var.client_name}"
  environment                      = "${var.environment}"
  subnet_id                        = "${module.vpc.private_vpc_subnet_range_db_az1_id}"
  security_groups                  = "${module.rds_secgroup.rds_security_group_id},${module.bastion_secgroup.bastion_security_group_id}"
  instance_type                    = "${var.instance_type}"
  server_role                      = "mongodb-rr"
  key_name                         = "${var.key_name}"
  mongo_data_disk_size             = "${var.mongo_data_disk_size}"
  mongo_journal_disk_size          = "${var.mongo_journal_disk_size}"
  mongo_logs_disk_size             = "${var.mongo_logs_disk_size}"
  user_data_script                 = "user_data_mongo.sh"
}

# Create single Elasticache Redis instance
module "elasticache_redis" {
  source                           = "../modules/elasticache_redis"
  client_name                      = "${var.client_name}"
  environment                      = "${var.environment}"
  subnet_id                        = "${module.vpc.private_vpc_subnet_range_db_az1_id},${module.vpc.private_vpc_subnet_range_db_az2_id}"
  security_groups                  = "${module.rds_secgroup.rds_security_group_id}"
  instance_type                    = "cache.t2.micro"
}

# Create the DB subnet group
module "db_subnet_group" {
  source                           = "../modules/db_subnet_group"
  client_name                      = "${var.client_name}"
  environment                      = "${var.environment}"
  subnet_id                        = "${module.vpc.private_vpc_subnet_range_db_az1_id},${module.vpc.private_vpc_subnet_range_db_az2_id}"
}

# This section to setup Cloudwatch metrics.  Still need to install Cloudwatch agent and configure to send logs.
module "cloudwatch_log_group_mongodb" {
  source                           = "../modules/cloudwatch_log_group"
  client_name                      = "${var.client_name}"
  environment                      = "${var.environment}"
  retention_in_days                = "30"
  cloudwatch_log_group_name        = "mongodb-group"
}

module "cloudwatch_log_group_web" {
  source                           = "../modules/cloudwatch_log_group"
  client_name                      = "${var.client_name}"
  environment                      = "${var.environment}"
  retention_in_days                = "30"
  cloudwatch_log_group_name        = "web-group"
}

module "cloudwatch_log_group_api" {
  source                           = "../modules/cloudwatch_log_group"
  client_name                      = "${var.client_name}"
  environment                      = "${var.environment}"
  retention_in_days                = "30"
  cloudwatch_log_group_name        = "api-group"
}

# # Create list of endpoints for configuration usage.  This writes a text file
# # to the directory where you are running Terraform.
# module "create_endpoint_list" {
#   source                 = "../modules/create_endpoint_list"
#   client_name            = "${var.client_name}"
#   environment            = "${var.environment}"
#   join_web_elb           = "${module.join_web.elb_dns_name}"
#   join_api_elb           = "${module.join_api.elb_dns_name}"
#   port_web_elb           = "${module.port_web.elb_dns_name}"
#   port_api_elb           = "${module.port_api.elb_dns_name}"
#   admin_web_elb          = "${module.admin_web.elb_dns_name}"
#   config_api_elb         = "${module.config_api.elb_dns_name}"
#   sftp_server_ext_ip     = "${module.sftp_server.server_public_ip}"
#   sftp_server_public_dns = "${module.sftp_server.server_public_dns}"
#   mongodb_master_ip      = "${module.mongodb_master.mongo_private_ip}"
#   mongodb_replica1_ip    = "${module.mongodb_replica1.mongo_private_ip}"
#   mongodb_replica2_ip    = "${module.mongodb_replica2.mongo_private_ip}"
#   redis_hostname         = "${module.elasticache_redis.redis_hostname}"
#   redis_port             = "${module.elasticache_redis.redis_port}"
# }

# These Cloudwatch log streams can be pre-created OR will be dynamically created when
# you install the "awslogs" package on the instances and configure.
# eg. In the /etc/awslogs/awslogs.conf file, if you specify a log group and log stream, 
#     they will be  dynamically created in Cloudwatch.  The only reason for pre-creating
#     them would be if you want some standardization and not have a free for all with naming.

# module "cloudwatch_log_stream_mongodb" {
#   source                           = "../modules/cloudwatch_log_stream"
#   client_name                      = "${var.client_name}"
#   environment                      = "${var.environment}"
#   cloudwatch_log_stream_name       = "mongodb-logs"
#   cloudwatch_log_group_name        = "${module.cloudwatch_log_group_mongodb.cloudwatch_log_group_name}"
# }

# module "cloudwatch_log_stream_web" {
#   source                           = "../modules/cloudwatch_log_stream"
#   client_name                      = "${var.client_name}"
#   environment                      = "${var.environment}"
#   cloudwatch_log_stream_name       = "web-logs"
#   cloudwatch_log_group_name        = "${module.cloudwatch_log_group_web.cloudwatch_log_group_name}"
# }

# module "cloudwatch_log_stream_api" {
#   source                           = "../modules/cloudwatch_log_stream"
#   client_name                      = "${var.client_name}"
#   environment                      = "${var.environment}"
#   cloudwatch_log_stream_name       = "api-logs"
#   cloudwatch_log_group_name        = "${module.cloudwatch_log_group_api.cloudwatch_log_group_name}"
# }


# This section to setup Cloudtrail logging to log all AWS API calls.
# module "cloudwatch_log_group_cloudtrail" {
#   source                           = "../modules/cloudwatch_log_group"
#   client_name                      = "${var.client_name}"
#   environment                      = "${var.environment}"
#   retention_in_days                = "30"
#   cloudwatch_log_group_name        = "cloudtrail-group"
# }

# module "cloudtrail" {
#   source                           = "../modules/cloudtrail"
#   region                           = "${var.region}"
#   client_name                      = "${var.client_name}"
#   environment                      = "${var.environment}"
#   s3_bucket_name                   = "${var.cloudtrail_s3_bucket_name}"
#   cloudwatch_log_group_name        = "${module.cloudwatch_log_group_cloudtrail.cloudwatch_log_group_name}"
# }



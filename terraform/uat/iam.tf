variable "generic_role_name" { default = "generic_role_name" }
variable "generic_instance_profile_name" { default = "generic_instance_profile" }


# Create IAM role for web
resource "aws_iam_role" "client_name_generic_role" {
  name               = "${var.client_name}_${var.generic_role_name}"
  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": "ec2.amazonaws.com",
        "Service": "vpc-flow-logs.amazonaws.com"
      },
      "Effect": "Allow",
      "Sid": ""
    }
  ]
}
EOF
}

# Create IAM policy
resource "aws_iam_policy" "client_name_generic_policy" {
  name        = "${var.client_name}_generic_policy"
  path        = "/"
  description = "${var.client_name}_generic_policy"
  policy = <<EOF
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Sid": "Stmt1477071439000",
            "Effect": "Allow",
            "Action": [
                "ec2:*"
            ],
            "Resource": [
                "*"
            ]
        },
        {
            "Sid": "Stmt1477071460000",
            "Effect": "Allow",
            "Action": [
                "s3:*"
            ],
            "Resource": [
                "*"
            ]
        },
        {
            "Sid": "Stmt1477071486000",
            "Effect": "Allow",
            "Action": [
                "acm:*"
            ],
            "Resource": [
                "*"
            ]
        },
        {
            "Sid": "Stmt1477071513000",
            "Effect": "Allow",
            "Action": [
                "elasticloadbalancing:*"
            ],
            "Resource": [
                "*"
            ]
        },
        {
            "Sid": "Stmt1477071530000",
            "Effect": "Allow",
            "Action": [
                "autoscaling:*"
            ],
            "Resource": [
                "*"
            ]
        },
        {
            "Sid": "Stmt1477071544000",
            "Effect": "Allow",
            "Action": [
                "rds:*"
            ],
            "Resource": [
                "*"
            ]
        }
    ]
}
EOF
}

# Create IAM policy for Cloudwatch
resource "aws_iam_policy" "client_name_cloudwatch_logs_policy" {
  name        = "${var.client_name}_cloudwatch_logs_policy"
  path        = "/"
  description = "${var.client_name}_cloudwatch_logs_policy"
  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Action": [
        "logs:CreateLogGroup",
        "logs:CreateLogStream",
        "logs:PutLogEvents",
        "logs:DescribeLogStreams"
    ],
      "Resource": [
        "arn:aws:logs:*:*:*"
    ]
  }
 ]
}
EOF
}

# Create IAM policy for VPC Flow Logs
resource "aws_iam_policy" "client_name_vpc_flow_logs_policy" {
  name        = "${var.client_name}_vpc_flow_logs_policy"
  path        = "/"
  description = "${var.client_name}_vpc_flow_logs_policy"
  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": [
        "logs:CreateLogGroup",
        "logs:CreateLogStream",
        "logs:PutLogEvents",
        "logs:DescribeLogGroups",
        "logs:DescribeLogStreams"
      ],
      "Effect": "Allow",
      "Resource": "*"
    }
  ]
}
EOF
}

# Create IAM policy for S3 Bucket Access
resource "aws_iam_policy" "client_name_s3_policy" {
  name        = "${var.client_name}_s3_logs_policy"
  path        = "/"
  description = "${var.client_name}_s3_logs_policy"
  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
      {
          "Effect": "Allow",
          "Action": "s3:ListAllMyBuckets",
          "Resource": "arn:aws:s3:::*"
      },
      {
          "Effect": "Allow",
          "Action": "s3:*",
          "Resource": [
              "arn:aws:s3:::nestwealth-ansible",
              "arn:aws:s3:::nestwealth-ansible/*"
          ]
      }
  ]
}
EOF
}

# Attach generic policy to role
resource "aws_iam_role_policy_attachment" "client_name_attach_generic_policy" {
  role       = "${aws_iam_role.client_name_generic_role.name}"
  policy_arn = "${aws_iam_policy.client_name_generic_policy.arn}"
}

# Attach cloudwatch policy to role
resource "aws_iam_role_policy_attachment" "client_name_attach_cloudwatch_logs_policy" {
  role       = "${aws_iam_role.client_name_generic_role.name}"
  policy_arn = "${aws_iam_policy.client_name_cloudwatch_logs_policy.arn}"
}

# Attach vpc flow logs policy to role
resource "aws_iam_role_policy_attachment" "client_name_attach_vpc_flow_logs_policy" {
  role       = "${aws_iam_role.client_name_generic_role.name}"
  policy_arn = "${aws_iam_policy.client_name_vpc_flow_logs_policy.arn}"
}

# Attach S3 policy to role
resource "aws_iam_role_policy_attachment" "client_name_attach_s3_policy" {
  role       = "${aws_iam_role.client_name_generic_role.name}"
  policy_arn = "${aws_iam_policy.client_name_s3_policy.arn}"
}

# Create instance profile
resource "aws_iam_instance_profile" "client_name_generic_instance_profile" {
  name  = "${var.client_name}_${var.generic_instance_profile_name}"
  roles = ["${aws_iam_role.client_name_generic_role.name}"]
}

# Create list of endpoints for configuration usage.  This writes a text file
# to the directory where you are running Terraform.
module "create_endpoint_list" {
  source                 = "../modules/create_endpoint_list"
  client_name            = "${var.client_name}"
  environment            = "${var.environment}"
  join_web_elb           = "${module.join_web.elb_dns_name}"
  join_api_elb           = "${module.join_api.elb_dns_name}"
  port_web_elb           = "${module.port_web.elb_dns_name}"
  port_api_elb           = "${module.port_api.elb_dns_name}"
  admin_web_elb          = "${module.admin_web.elb_dns_name}"
  config_api_elb         = "${module.config_api.elb_dns_name}"
  sftp_server_ext_ip     = "${module.sftp_server.server_public_ip}"
  sftp_server_public_dns = "${module.sftp_server.server_public_dns}"
  mongodb_master_ip      = "${module.mongodb_master.mongo_private_ip}"
  mongodb_replica1_ip    = "${module.mongodb_replica1.mongo_private_ip}"
  mongodb_replica2_ip    = "${module.mongodb_replica2.mongo_private_ip}"
  redis_hostname         = "${module.elasticache_redis.redis_hostname}"
  redis_port             = "${module.elasticache_redis.redis_port}"
}

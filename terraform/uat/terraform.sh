#!/bin/bash

# This is a wrapper script for Terraform to ensure environment and remote state is set correctly.

export PATH="/usr/local/terraform/bin:${PATH}"

# Check for number of arguments
if [ "$#" -ne 3 ]; then
  echo $"Usage: $0 {get|plan|apply|destroy} {client_name} {key_name}"

else

  # Set some variables
  ENVIRONMENT="production"
  REGION="us-east-1"
  CLIENT_NAME="$2"
  KEYPAIR_NAME="$3"
  BUCKET="${CLIENT_NAME}-remote_state-${ENVIRONMENT}"
  STATE_FILE="terraform.tfstate"

  echo "Grabbing Terraform state file from ${BUCKET}"

  # Set remote state
  terraform remote config -backend=s3 -backend-config="region=${REGION}" -backend-config="bucket=${BUCKET}" -backend-config="key=${STATE_FILE}"

  case "$1" in
    get)
      terraform get
      ;;
    plan)
      echo "###########################"
      echo "Running Terraform get first"
      echo "###########################"
      terraform get
      echo " "
      terraform plan -var "client_name=${CLIENT_NAME}" -var "key_name=${KEYPAIR_NAME}"
      ;;
    apply)
      terraform apply -var "client_name=${CLIENT_NAME}" -var "key_name=${KEYPAIR_NAME}"
      ;;
    destroy)
      terraform destroy
      ;;
    *)
      echo $"Usage: $0 {get|plan|apply|destroy} {client_name} {key_name}"
      echo 1
  esac
fi
